var express = require('express');
var rendr = require('rendr');
var rendr_middleware = require('rendr/server/middleware');
var rendr_engine = require('rendr/server/viewEngine');

var app = express();
app.set('views', __dirname + '/app/views');
app.set('view engine', 'js');
app.engine('js', rendr_engine);
app.use(express.compress());
app.use(express.static(__dirname + '/public'));
app.use(express.logger());
app.use(express.bodyParser());
app.use(app.router);

rendr.server.init({ dataAdapter: { } }, function (err) {

  app.use('/api', rendr_middleware.initApp());
  app.use('/api', rendr_middleware.apiProxy());

  var routes = rendr.server.router.buildRoutes();
  routes.forEach( function (args) {
    var path = args.shift();
    var definition = args.shift();
    app.get(path, rendr_middleware.initApp());
    app.get(path, args);
  });

  app.listen(process.env.OPENSHIFT_NODEJS_PORT || 3000,
    process.env.OPENSHIFT_NODEJS_IP || 'localhost', function () {
      console.log('server started');
  });

});