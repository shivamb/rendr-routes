var BaseView = require('./base_view');

module.exports = BaseView.extend({
  className: 'more_index_view'
});
module.exports.id = 'MoreIndexView';