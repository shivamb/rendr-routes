var BaseView = require('./base_view');

module.exports = BaseView.extend({
  className: 'routes_index_view'
});
module.exports.id = 'RoutesIndexView';