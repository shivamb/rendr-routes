module.exports = function(Handlebars) {

var templates = {};

templates["home_index_view"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "<h1>Hello Render with Routes!</h1>\nGo to route <a href='/more'>/more</a><br>\nGo to route <a href='/routes'>/routes</a><br>\nCurrent route: /";
  });

templates["more_index_view"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "<h1>Hello Render with Routes!</h1>\nGo to route <a href='/'>/</a><br>\nGo to route <a href='/routes'>/routes</a><br>\nCurrent route: /more";
  });

templates["routes_index_view"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "<h1>Hello Render with Routes!</h1>\nGo to route <a href='/'>/</a><br>\nGo to route <a href='/more'>/more</a><br>\nCurrent route: /routes";
  });

return templates;

};